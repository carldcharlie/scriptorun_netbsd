function tankzero
	for DISK in (sysctl -n hw.disknames | tr ' ' '\n')
		disklabel -r $DISK 2>/dev/null | grep TankZero >/dev/null && set TARGET $DISK
	end

	switch $argv
	case on
		doas mount /dev/{$TARGET}e /media/tankzero 
	case off 
		doas umount /media/tankzero	
	case "*"
		echo "Error: sintaxis must be -> tankzero on|off"
	end
end
