function tanktwo
	for DISK in (sysctl -n hw.disknames | tr ' ' '\n')
		disklabel -r $DISK 2>/dev/null | grep TankTWO >/dev/null && set TARGET $DISK
	end

	switch $argv
	case on
		doas mount -t ext2fs /dev/{$TARGET}i /media/tanktwo	
	case off 
		doas umount /media/tanktwo	
	case "*"
		echo "Error: sintaxis must be -> tanktwo on|off"
	end
end
