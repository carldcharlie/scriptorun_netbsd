function tankone
	for DISK in (sysctl -n hw.disknames | tr ' ' '\n')
		disklabel -r $DISK 2>/dev/null | grep TankOne >/dev/null && set TARGET $DISK
	end

	switch $argv
	case on
		doas mount /dev/{$TARGET}e /media/tankone	
	case off 
		doas umount /media/tankone	
	case "*"
		echo "Error: sintaxis must be -> tankone on|off"
	end
end
