function fish_prompt
	set_color yellow
	printf "$USER "
	set_color cyan
	printf "%s " (hostname) 
	set_color green
	printf "%s " (pwd)
	set_color white
	printf "\n> " 
end
