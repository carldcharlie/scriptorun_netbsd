set PATH {$HOME}/bin:/bin:/sbin:/usr/bin:/usr/sbin:/usr/X11R7/bin:/usr/local/bin:/usr/local/sbin:/usr/pkg/bin:/usr/pkg/sbin:/usr/games
# Check system messages
# msgs -q
# Allow terminal messages
# mesg y

########################################################################
if status is-interactive
    # Commands to run in interactive sessions can go here
	set BOLD (tput bold)
        set BLACK '\033[30m'
        set RED '\033[31m'
        set GREEN '\033[32m'
        set YELLOW '\033[33m'
        set BLUE '\033[34m'
        set MAGENTA '\033[35m'
        set CYAN '\033[36m'
        set WHITE '\033[37m'
        set RESET '\033[0m'
end
